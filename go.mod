module gitlab.com/Cacophony/API

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/agnivade/levenshtein v1.1.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jinzhu/gorm v1.9.8
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser/v2 v2.1.0
	gitlab.com/Cacophony/go-kit v0.0.0-20191021180731-31da95901b42
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

go 1.13
