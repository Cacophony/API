package main

import (
	"gitlab.com/Cacophony/go-kit/errortracking"
	"gitlab.com/Cacophony/go-kit/logging"
)

type config struct {
	Port               int                  `envconfig:"PORT" default:"8000"`
	Hash               string               `envconfig:"HASH"`
	Environment        logging.Environment  `envconfig:"ENVIRONMENT" default:"development"`
	ClusterEnvironment string               `envconfig:"CLUSTER_ENVIRONMENT" default:"development"`
	ErrorTracking      errortracking.Config `envconfig:"ERRORTRACKING"`
	ProcessorBaseURL   string               `envconfig:"PROCESSOR_BASE_URL" default:"http://processor"`
	RedisAddress       string               `envconfig:"REDIS_ADDRESS" default:"localhost:6379"`
	RedisPassword      string               `envconfig:"REDIS_PASSWORD"`
	DBDSN              string               `envconfig:"DB_DSN" default:"postgres://postgres:postgres@localhost:5432/?sslmode=disable"`
	GraphQLSandbox     bool                 `envconfig:"GRAPHQL_SANDBOX"`
}
