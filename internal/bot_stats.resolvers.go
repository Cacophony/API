package internal

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
)

func (r *botStatsResolver) Guilds(ctx context.Context, obj *BotStats) (*int, error) {
	count, err := r.state.AllGuildsCount()
	if err != nil {
		return nil, err
	}

	return &count, nil
}

func (r *botStatsResolver) Channels(ctx context.Context, obj *BotStats) (*int, error) {
	count, err := r.state.AllChannelsCount()
	if err != nil {
		return nil, err
	}

	return &count, nil
}

func (r *botStatsResolver) Users(ctx context.Context, obj *BotStats) (*int, error) {
	count, err := r.state.AllUsersCount()
	if err != nil {
		return nil, err
	}

	return &count, nil
}

func (r *botStatsResolver) CommandsExecuted(ctx context.Context, obj *BotStats) (*int, error) {
	var metricCounter struct {
		Value int
	}

	err := r.db.
		Table("metrics_counters").
		Where("key = ?", "total_commands").
		Select("value").
		First(&metricCounter).
		Error
	if err != nil {
		return nil, err
	}

	return &metricCounter.Value, nil
}

func (r *botStatsResolver) MessagesReceived(ctx context.Context, obj *BotStats) (*int, error) {
	var metricCounter struct {
		Value int
	}

	err := r.db.
		Table("metrics_counters").
		Where("key = ?", "total_messages_received").
		Select("value").
		First(&metricCounter).
		Error
	if err != nil {
		return nil, err
	}

	return &metricCounter.Value, nil
}

// BotStats returns BotStatsResolver implementation.
func (r *Resolver) BotStats() BotStatsResolver { return &botStatsResolver{r} }

type botStatsResolver struct{ *Resolver }
