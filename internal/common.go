package internal

import (
	"fmt"
	"strings"
)

func (resolver *Resolver) processorEndpoint(endpoint string) string {
	return fmt.Sprintf("%s/%s", resolver.processorBaseURL, strings.TrimLeft(endpoint, "/"))
}
