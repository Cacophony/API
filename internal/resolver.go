package internal

//go:generate go run github.com/99designs/gqlgen

import (
	"strings"

	"github.com/jinzhu/gorm"
	"gitlab.com/Cacophony/go-kit/state"
) // THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

type Resolver struct {
	processorBaseURL string
	state            *state.State
	db               *gorm.DB
}

func NewResolver(processorBaseURL string, state *state.State, db *gorm.DB) *Resolver {
	return &Resolver{
		processorBaseURL: strings.TrimRight(processorBaseURL, "/"),
		state:            state,
		db:               db,
	}
}
