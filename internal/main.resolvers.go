package internal

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func (r *queryResolver) BotStats(ctx context.Context) (*BotStats, error) {
	return &BotStats{}, nil
}

func (r *queryResolver) PluginHelp(ctx context.Context) (*PluginHelp, error) {
	resp, err := http.Get(r.processorEndpoint("/plugins/help/commands"))
	if err != nil || resp.StatusCode != http.StatusOK {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var pluginHelp []*PluginHelpPlugin
	err = json.Unmarshal(data, &pluginHelp)
	if err != nil {
		return nil, err
	}

	return &PluginHelp{Plugins: pluginHelp}, nil
}

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
